package challenge.juanpabloochoa.personEngine.interfaces;

public interface PersonsQuery {
	String getName();
	String getLastName();
}
