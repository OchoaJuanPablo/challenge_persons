package challenge.juanpabloochoa.personEngine.model;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
@Entity
@Table(name="persons_table")
public class Person implements Serializable{
	private static final long serialVersionUID = 1L;

	public Person() {
		
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_person")
	private Integer id;
	@Column(name="name_person")
	private String name;
	@Column(name="lastName_person")
	private String lastName;
    @JsonFormat(pattern="yyyy-MM-DD",shape=Shape.STRING)
	@Column(name="birthDate_person")
	private String birthDate;
	@Column(name="month_person")
	private Integer monthPerson;
	@Column(name="age_person")
	private Integer agePerson;
	@Column(name="dni_person")
	private Long dni;
	
	public Integer getMonth() {
		return monthPerson;
	}
	public void setMonth(Integer month) {
		this.monthPerson = month;
	}
	public Integer getAgePerson() {
		return agePerson;
	}
	public void setAgePerson(Integer agePerson) {
		this.agePerson = agePerson;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthdate() {
		return birthDate;
	}
	public void setBirthdate(String birthdate) {
		this.birthDate = birthdate;
	}
	
	public Long getDni() {
		return dni;
	}
	public void setDni(Long dni) {
		this.dni = dni;
	}
	

	
}
