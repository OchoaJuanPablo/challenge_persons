package challenge.juanpabloochoa.personEngine;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonEngineApplication.class, args);
	}

}
